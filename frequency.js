function randomColor() {
    var rgb = [],
        i = 0;

    while (i <= 2) {
        rgb.push(Math.floor(Math.random() * 255));
        i++;
    }
    return 'rgb(' + rgb[0] + ',' + rgb[1] + ',' + rgb[2] + ')';
};

const wordChecker = function (newWord, countWord) {
    for (let i = 0; i < newWord.length; i++) {
        if (countWord[newWord[i]] === undefined) {
            countWord[newWord[i]] = 1;
        } else {
            countWord[newWord[i]]++;
        }
    }
}

const countLetter = function (word) {
    let newWord = word.replace(/[^a-zA-Z]/g, "").toLowerCase().split('').sort();
    let countWord = {};
    wordChecker(newWord, countWord);

    for (const key in countWord) {
        console.log(key + ' = ' + countWord[key]);
        const newElement = document.createElement("span");
        newElement.className = "bar";
        newElement.style.backgroundColor = "#92a8d1";
        const newText = document.createTextNode('"'+ key +'"' + ' : ' + countWord[key]);
        newElement.appendChild(newText);
        const destination = document.getElementById("lettersDiv");
        destination.appendChild(newElement);
    }
}

const countWord = function (word) {
    let newWord = word.replace(/[^a-zA-z'\s]+/g, " ").toLowerCase().split(' ');
    let countWord = {};
    wordChecker(newWord, countWord);

    for (const key  in countWord) {
        console.log(key + ' = ' + countWord[key]);
        const newElement = document.createElement("span");
        newElement.className = "bar";
        newElement.style.backgroundColor = '#dddddd';
        const newText = document.createTextNode(key + ' : ' + countWord[key]);
        newElement.appendChild(newText);
        const destination = document.getElementById("wordsDiv");
        destination.appendChild(newElement);
    }
}

function clearContents(element) {
    element.value = '';
  }
const onClickCount = function () {
    let typedText = document.getElementById("textInput").value;
    (countLetter(typedText));
    (countWord(typedText));

}



// countWord("Lastly, you need a way to ignore capitalization and punctuation. In a sentence like 'Dogs enjoy sniffing other dogs!' you want both occurrences of the word 'dogs' to be counted the same, even though one is capitalized and one isn't. Likewise, you don't care that one has an exclamation point and the other doesn't.");

// countLetter("I guess I asked it wrong, I want to leave all numbers in the string, but only strip special characters like I guess I asked it wrong, I want to leave all numbers in the string, but only strip special characters like ! ");